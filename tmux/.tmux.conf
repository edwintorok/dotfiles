# based on example.tmux.conf

# Change the default $TERM to screen-256color
# tmux-256color would be better but is not supported on old systems over
# ssh/etc.
set -g default-terminal "screen-256color"

# does not work with iTerm2
# setw -g aggressive-resize on

# Enable RGB colour if running in xterm(1)
set-option -sa terminal-overrides ",xterm-256color*:Tc"

# Turn the mouse on
# Will need to use SHIFT{drag,middle click} to interact with X clipboard
# C-b = for tmux paste buffer
set -g mouse on

# Some tweaks to the status line
#set -g status-right "%H:%M"
set -g window-status-current-style "underscore"

# No bells at all
set -g bell-action none

# Set larger history limit
set -g history-limit 30000

# Update env vars on re-attach, e.g. SSH_AUTH_SOCK
set -g update-environment "SSH_ASKPASS SSH_AGENT_PID SSH_AUTH_SOCK SSH_CONNECTION"

# set xterm title
set -g set-titles on
set -g set-titles-string "#T"

setw -g monitor-activity on
set -g visual-activity on

# suggested by neovim
set-option -sg escape-time 10

# toggle synchronized panes
bind y set synchronize-panes\; display 'synchronize-panes #{?synchronize-panes,on,off}'

# reload tmux.conf changes
bind r source-file ~/.tmux.conf

# neovim recommended
set-option -g focus-events on
