.PHONY: all update remote-update simulate install install-nogui uninstall clean-plugins

all: update install

update:
	git submodule sync
	git submodule update --init --recursive

remote-update:
	git submodule update --remote --recursive

simulate: update
	stow -Sv */

clean-plugins:
	rm -rf ~/.local/share/nvim/site/pack/packer/opt/* ~/.local/share/nvim/site/pack/packer/start/*
	rm -f ~/.config/nvim/plugin/packer_compiled.lua ~/.cache/nvim/luacache* ~/.local/share/nvim/rplugin.nvim

# no more remote plugins, and helptags is done by packer
# not using PackerSync here to obey snapshots
# python3 -m pip install neovim-remote
install-nogui:
	stow -v */
	rm -f ~/.config/nvim/plugin/packer_compiled.lua ~/.cache/nvim/luacache* ~/.local/share/nvim/rplugin.nvim
	nvim --headless -c 'autocmd User PackerComplete quitall' \
		 -c 'PackerSync'
	echo "OK"


FONTPATH=vim/.local/share/fonts

$(FONTPATH)/%.ttf: Go-Mono/%.ttf
	cp $< $*
	# could change name but would have to change filename to match for Mac OS
	# it is already renamed, so not necessary
	ttx -i -o - $* | grep -v 'BOX DRAWINGS' >$*.ttx
	ttx -b -o $@ $*.ttx
	rm -f $*.ttx $*

install-fonts: $(addprefix $(FONTPATH)/,\
	Go-Mono-Bold-Italic-Nerd-Font-Complete.ttf\
	Go-Mono-Bold-Nerd-Font-Complete.ttf\
	Go-Mono-Italic-Nerd-Font-Complete.ttf\
	Go-Mono-Nerd-Font-Complete.ttf\
	LICENSE.go-mono-fonts)
	stow -v vim/

install: install-nogui install-fonts
	echo "OK"

uninstall:
	stow -Dv */

# could've added quitall but useful to be able to review changes
# press 'q' :qa after reviewing the changes
SNAPSHOT=vim/.config/nvim/snapshots/default
$(SNAPSHOT):
	nvim -c 'autocmd User PackerComplete PackerSnapshot default' -c 'PackerSync'
	jq --sort-keys . <$(SNAPSHOT) >$(SNAPSHOT).tmp
	mv $(SNAPSHOT).tmp $@

update-packer:
	rm -f $(SNAPSHOT)
	$(MAKE) $(SNAPSHOT)
