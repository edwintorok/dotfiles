Run `make simulate` to update and simulate symlink creation, then
run `make all` to update and install all dotfile symlinks. To undo run `make uninstall`.

To try out without modifying the homedir:
```
mkdir dotfiles-test && cd dotfiles-test
git clone --recursive https://gitlab.com/edwintorok/dotfiles.git dotfiles
cd dotfiles
make install
cd ..
export HOME=`pwd`
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
zsh -l
```
