autoload -Uz zrecompile
ZSH_DOTDIR=~/dotfiles/zsh
OPAMINIT=~/.opam/opam-init
ZSH_FILES=(\
    ${ZSH_DOTDIR}/agkozak-zsh-prompt/agkozak-zsh-prompt.plugin.zsh\
    ${ZSH_DOTDIR}/agkozak-zsh-prompt/prompt_agkozak-zsh-prompt_setup\
    ${ZSH_DOTDIR}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh\
    ${ZSH_DOTDIR}/zsh-syntax-highlighting/highlighters/*/*.zsh\
    ${OPAMINIT}/*.zsh\
    ${OPAMINIT}/variables.sh
)
zrecompile -q -p \
    -R ~/.zshenv -- \
    -R ~/.zshrc -- \
    -R ~/.zlogin -- \
    -M ${ZCACHE}/zcompdump -- \
    -U ~/dotfiles/zsh/dircolors.sh -- \
    ${^ZSH_FILES}(P:-U:P:-z:^P:--:) &!
