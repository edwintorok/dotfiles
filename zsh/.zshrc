# Set directories {{{
: ${XDG_DATA_HOME:=${HOME}/.local/share}
: ${XDG_CACHE_HOME:=${HOME}/.cache}
# cache should per host if shared via NFS
# use HOSTNAME if set so that zsh-bench doesn't create a new cache
# for each instance
ZCACHE="${XDG_CACHE_HOME}/zsh-${HOSTNAME-${HOST}}"
[[ -d "${ZCACHE}" ]] || mkdir -p "${ZCACHE}"

# }}}

# Set history options {{{
# use a large history
HISTFILE=${XDG_DATA_HOME}/zsh_history
HISTSIZE=1000000000
SAVEHIST=$HISTSIZE

# append commands immediately to history with timestamps and make them visible in other shells
# avoids loosing history with multiple shells open
setopt share_history

# do not corrupt history on NFS
# unfortunately sometimes NFS locking becomes broken and starting a login shell hangs forever
# so do not set this option for now
# setopt hist_fcntl_lock

# do not record duplicate commands
setopt hist_ignore_all_dups

# remove unneeded blanks
setopt hist_reduce_blanks

# if you do not want an entry added to the history, start it with a space
setopt hist_ignore_space

# do not immediately run commands expanded from !!, allow editing them first
setopt hist_verify

# do not store the history command itself and func definitions
setopt hist_no_store
setopt hist_no_functions
#}}}

# Set up completion {{{

# from the recommended config in /etc/zsh
# process completion for kill
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

zstyle ':completion:*' accept-exact-dirs true

# enable caching
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ${ZCACHE}/zcompcache

# ignore useless completions
zstyle ':completion:*:files' ignored-patterns '*?.o' '*?~' '*.zwc'
zstyle ':completion:*:functions-non-comp' ignored-patterns '_*'
zstyle ':completion:*:functions' ignored-patterns '(_*|pre(cmd|exec)|prompt_*)'

# manual page completion
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.*' insert-sections true

# complete http://www/~user from ~user/public_html
zstyle ':completion:*:urls' local www '' public_html

# The following lines were added by compinstall

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' format 'Completing %B%d%b'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent pwd ..
zstyle ':completion:*' insert-unambiguous false
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=** r:|=** l:|=*'
zstyle ':completion:*' max-errors 1
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' prompt '%o (errors: %e)'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

# turn this off on NFS
# zstyle ':completion:*' rehash true

zstyle :compinstall filename ~/.zshrc

# need to setup styles before calling compinit if menu and expansion are used
autoload -Uz compinit

# modified location of completion dump
# TODO: could use -C here if we have a way to flush the cache
compinit -d ${ZCACHE}/zcompdump
# End of lines added by compinstall

# Completion options {{{
setopt correct

export CORRECT_IGNORE='_*'

# setopt correct_all
setopt auto_menu
setopt always_to_end
setopt auto_list
setopt complete_in_word

# add slash when completing dirs and don't remove it
setopt auto_param_slash
setopt no_auto_remove_slash

setopt case_glob
# }}}
# }}}

# Set up colors {{{
autoload -Uz colors
colors

zstyle ':completion:*:corrections' format ' %F{green}-- %d (errors: %e) --%f'
zstyle ':completion:*:descriptions' format ' %F{yellow}-- %d --%f'
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
#zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'

# from the recommended config
# dircolors -b >dircolors.sh
if (($+commands[dircolors])); then
    source ~/dotfiles/zsh/dircolors.sh
    zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
else
    alias ls='ls -CF'
fi

if (($+commands[git-branchless])); then
    alias git='git-branchless wrap --'
fi

if (($+commands[nvim])); then
    export VISUAL=nvim
fi

if [[ (($+commands[nvr])) && (-n "${NVIM}" || -n "${NVIM_LISTEN_ADDRESS}") ]]; then
    export VISUAL="nvr -cc split --remote-wait +'setlocal bufhidden=delete'"
    alias nvim=${VISUAL}
fi

alias vim=${VISUAL}
alias vi=${VISUAL}
alias vimdiff="${VISUAL} -d"

function ssh() {
    if [[ (-n "${title_tsl}") && (-n "${title_fsl}") ]]; then
        print -n "${title_tsl}"
        print -nR $*
        print -n "${title_fsl}"
    fi
    command ssh $*
}

#}}}

# Set up the prompt {{{

autoload -Uz add-zsh-hook

# Terminal specific workarounds {{{
zmodload zsh/terminfo
if [[ $terminfo[hs] = 'yes' ]]; then
    title_tsl=$terminfo[tsl]
    title_fsl=$terminfo[fsl]
fi

_orig_TERM="${TERM}"
if [[ "$TERM" = xterm* ]]; then
    # xterm supports setting title, but not in the default TERM
    # gnome-terminal now advertises xterm-256 on Fedora 37,
    # and terminfo[hs] is yes
    # so apply workaround
    # however the extra ncurses package with xterm+sl terminfo might not be installed,
    # so hardcode the output here
    #title_tsl=$(TERM=xterm+sl echoti tsl)
    #title_fsl=$(TERM=xterm+sl echoti fsl)
    title_tsl='\e]0;'
    title_fsl='\a'
elif [[ "$TERM" = screen* && -n "$TMUX" ]]; then
    # tmux support ctrl-arrows, but for compatibility with ssh to old systems
    # we can't use TERM=tmux-256color globally
    TERM=tmux-256color
fi

# Set up keybindings {{{
# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e
typeset -A key

# from the zsh wiki, setup key bindings according to terminfo
# Debian already does this, but CentOS doesn't

#these would be missing over ssh on old systems, or for TERM=stterm
#hardcode :(
key[CtrlLeft]=$terminfo[kLFT5]
if [[ -z $key[CtrlLeft] ]]; then
    key[CtrlLeft] = '\e[1;5D.'
fi
key[CtrlRight]=$terminfo[kRIT5]
if [[ -z $key[CtrlRight] ]]; then
    key[CtrlRight] = '\e[1;5C.'
fi

key[Home]="$terminfo[khome]"
key[End]="$terminfo[kend]"
key[Insert]="$terminfo[kich1]"
key[Backspace]="$terminfo[kbs]"
key[Delete]="$terminfo[kdch1]"
key[Up]="$terminfo[kcuu1]"
key[Down]="$terminfo[kcud1]"
key[Left]="$terminfo[kcub1]"
key[Right]="$terminfo[kcuf1]"
key[PageUp]="$terminfo[kpp]"
key[PageDown]="$terminfo[knp]"

# setup key accordingly
[[ -n "$key[Home]"      ]] && bindkey -- "$key[Home]"      beginning-of-line
[[ -n "$key[End]"       ]] && bindkey -- "$key[End]"       end-of-line
[[ -n "$key[Insert]"    ]] && bindkey -- "$key[Insert]"    overwrite-mode
[[ -n "$key[Backspace]" ]] && bindkey -- "$key[Backspace]" backward-delete-char
[[ -n "$key[Delete]"    ]] && bindkey -- "$key[Delete]"    delete-char
[[ -n "$key[Up]"        ]] && bindkey -- "$key[Up]"        up-line-or-history
[[ -n "$key[Down]"      ]] && bindkey -- "$key[Down]"      down-line-or-history
[[ -n "$key[Left]"      ]] && bindkey -- "$key[Left]"      backward-char
[[ -n "$key[Right]"     ]] && bindkey -- "$key[Right]"     forward-char

# my additions
[[ -n "$key[CtrlLeft]" ]] && bindkey -- "$key[CtrlLeft]" backward-word
[[ -n "$key[CtrlRight]" ]] && bindkey -- "$key[CtrlRight]" forward-word

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
            emulate -L zsh
            printf '%s' ${terminfo[smkx]}
    }
    function zle-line-finish () {
            emulate -L zsh
            printf '%s' ${terminfo[rmkx]}
    }
    zle -N zle-line-init
    zle -N zle-line-finish
fi
# }}}

TERM="${_orig_TERM}"
# }}}

# based on zsh-lovers(1)
# set xterm/tmux title
function title {
    if [[ (-n "${title_tsl}") && (-n "${title_fsl}") ]]; then
        print -n "${title_tsl}"
        print -nP $'%n@%m: '
        print -nR $*
        print -n "${title_fsl}"
    fi
}

function title_precmd { title zsh "$PWD" }
function title_preexec {
    emulate -L zsh
    title $1
}
add-zsh-hook precmd title_precmd
add-zsh-hook preexec title_preexec

# for Emacs/tramp mode so it can find the prompt
[[ $TERM == "dumb" ]] && unsetopt zle

fpath+=(~/dotfiles/zsh/agkozak-zsh-prompt)
autoload -Uz promptinit
promptinit

AGKOZAK_PROMPT_DIRTRIM_STRING=$'\u2026'
AGKOZAK_PROMPT_CHAR=( ❯ ❯ ❮ )
AGKOZAK_CUSTOM_SYMBOLS=( '⇣⇡' '⇣' '⇡' '✚' 'x' '!' '>' '?' 'S')
AGKOZAK_MULTILINE=0
AGKOZAK_CMD_EXEC_TIME=1
hourglass=$'\u231b'
# nicely falls back if TERM is not 256 color
AGKOZAK_COLORS_PATH=32 # blue
AGKOZAK_COLORS_PROMPT_CHAR=254
AGKOZAK_COLORS_USER_HOST=252
AGKOZAK_CMD_EXEC_TIME_CHARS=("${hourglass}" '')

_opam_switch_hook() {
    # we may or may not use the env hook script
    # show the actually active env, not based on current dir
    # so rely only on env vars
    # set CONDA_DEFAULT_ENV here, because setting VIRTUAL_ENV interferes with Python's:
    # it'll think a python venv is active there and won't activate another.
    # note that with auto-env switching this is delayed by one command
    if [[ ${OPAM_SWITCH_PREFIX:t} == '_opam' ]]; then
        CONDA_DEFAULT_ENV="${OPAM_SWITCH_PREFIX:h:t}"
    elif [ -n "$OPAM_SWITCH_PREFIX" ]; then
        CONDA_DEFAULT_ENV="${OPAM_SWITCH_PREFIX:t}"
    else
        unset CONDA_DEFAULT_ENV
    fi
}
typeset -ag precmd_functions;
if [[ -z ${precmd_functions[(r)_opam_switch_hook]} ]]; then
    precmd_functions+=_opam_switch_hook;
fi

if [ -f /run/.containerenv ] && [ -f /run/.toolboxenv ]; then
    AGKOZAK_PRE_PROMPT_CHAR=" %F{magenta}⬢%f "
else
    AGKOZAK_PRE_PROMPT_CHAR=' '
fi

prompt agkozak-zsh-prompt

# preserve partial output lines without newline like: echo -n foo
setopt prompt_cr # agkozak disables this, reenable
setopt prompt_sp

# needed for the above prompt to work
# already set by agkozak
#setopt prompt_subst

# only show rprompt on current line
setopt transient_rprompt

# set vcs_info, based on zshcontrib(1)
zstyle ':vcs_info:*' actionformats \
    $'%F{magenta}[%F{green}\u2387 %b%F{yellow}|%F{red}%a%F{magenta}]%f '
zstyle ':vcs_info:*' formats       \
    $'%F{magenta}[%F{green}\u2387 %b%F{magenta}]%f '
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' use-quilt false
zstyle ':vcs_info:*' check-for-staged-changes false

#}}}

# Set sensible default behaviour {{{

# quiet
setopt no_beep

# Immediately notify when background job finishes
setopt notify

# pipes do not override redirects
# the output of the following is empty:
# echo foo >/dev/null | cat
setopt nomultios

# exit status of first failed command in a pipeline
autoload -Uz is-at-least
if is-at-least 5.0.3; then
    setopt pipe_fail
fi

# Do not overwrite files with redirects, override with >|
setopt noclobber

# print PIDs in job list
setopt long_list_jobs

# directory stack
setopt auto_pushd
setopt pushd_silent
setopt pushd_to_home
setopt pushd_ignore_dups
DIRSTACKSIZE=20

# handle URL pasting correctly
autoload -Uz bracketed-paste-url-magic
zle -N bracketed-paste bracketed-paste-url-magic
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

# Freeze terminal settings, prevents runaway processes from messing up the terminal
ttyctl -f

# load helpers
autoload -U edit-command-line && zle -N edit-command-line
bindkey '^xe' edit-command-line
bindkey '^x^e' edit-command-line
bindkey -M vicmd v edit-command-line

autoload -Uz run-help
autoload -Uz run-help-git
autoload -Uz run-help-ip
autoload -Uz run-help-sudo

# run command in fake pty
zmodload -a zsh/zpty zpty

# fast ls without stat calls, sorting and colors
alias lsu='command ls -aU'
# don't show all the snaps
alias dfh='df -h -x squashfs -x tmpfs'

#alias chromium='(ulimit -Sv 2500000 && chromium)'
#alias firefox='(ulimit -Sv  6000000 && firefox)'
alias codium='codium --enable-features=UseOzonePlatform --ozone-platform=wayland'

# resume background job of single word instead of starting new command
set auto_resume

[[ -s /etc/zsh_command_not_found ]] && . /etc/zsh_command_not_found
#}}}

# Set env vars {{{
# Automatically decompress some files when viewed with less
(($+commands[lesspipe])) && eval $(lesspipe)

# OPAM configuration
if [[ -s ~/.opam/opam-init/init.zsh ]]; then
    . ~/.opam/opam-init/init.zsh || true
fi
typeset -U path MANPATH
#}}}

export OPAMJOBS=8

ulimit -s 16384

# Setup highlighting {{{
# Must be last according to its docs
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)
#SYNTAX=/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
SYNTAX=~/dotfiles/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[[ -s ${SYNTAX} ]] && . ${SYNTAX}
return 0
# }}}
# vim:filetype=zsh foldmethod=marker foldlevel=0
