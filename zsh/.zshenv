# for debian/ubuntu: avoid initializing twice, we initialize this in .zshrc
skip_global_compinit=true

export LANG=en_GB.UTF-8

# for OpenBSD
export LC_CTYPE=en_GB.UTF-8
export LESSCHARSET=utf-8

# note: not UTF-8, speeds up grep/sort, TODO: is it still needed?
export LC_COLLATE=C

path=($HOME/.cargo/bin $HOME/.local/bin $path)

# Emacs complains if PATH/MANPATH is different between interactive and
# non-interactive shells
if [[ -s ~/.opam/opam-init/variables.sh ]]; then
    . ~/.opam/opam-init/variables.sh || true
fi
typeset -U path MANPATH
export PATH

#if [[ "$TERM" = "xterm" ]]; then
#    TERM=xterm-256color
#fi
export EDITOR=vim

# we'll do it ourselves in .zshrc
export DEBIAN_PREVENT_KEYBOARD_CHANGES=1

# for opam: due to sandboxing it won't see my .config/dune/config
export DUNE_CACHE=enabled
