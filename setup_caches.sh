#!/bin/sh
# https://www.jwillikers.com/btrfs-layout
# these are all caches, but instead of subvolumes
# (where we can't change mount options anyway) just chattr +C.
# Note that previously this used to put things into /var/tmp, but that conflicts
# with the automatic /var/tmp cleaner after a month corrupting .opam/etc.
set -eu

# .mozilla is not a cache, but it has a SQLite file that can experience performance issues on CoW
for USERDIR in\
    .cache\
    Downloads\
    .opam .cargo .stack .mozilla .var go .npm .esy
do
    SRC="${HOME}/${USERDIR}"
    if [ -d "${SRC}" ]; then
        echo "Moving ${SRC} to ${SRC}.old"
        mv "${SRC}/" "${SRC}.old/"
        mkdir "${SRC}"
        chattr +C "${SRC}"
        cp -a --reflink=never "${SRC}.old/." "${SRC}"
        rm "${SRC}.old" -rf
    else
        echo "Creating ${SRC}"
        mkdir -p "${SRC}"
        chattr +C "${SRC}"
    fi
done
