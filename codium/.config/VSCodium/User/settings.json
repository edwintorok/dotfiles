{
    "[markdown]": {
        // Set this to false if you turn on quickSuggestions
        // but don't want suggestions for markdown related snippets
        // as you type:
        "editor.suggest.showSnippets": false,
        // quickSuggestions true will provide suggestions as you type.
        // If you turn this on and DO NOT want suggestions
        // for non-wiki-link, non-tag words,
        "editor.quickSuggestions": {
            "comments": "on",
            "strings": "on",
            "other": "on"
        },
        "editor.tabSize": 2,
        // This is poorly documented, but seems to offer suggestions
        // from any word in open document when turned on, which
        // can be a little distracting in markdown docs:
        "editor.wordBasedSuggestions": false
    },
    "[ocaml]": {
        // word based suggestions are distracting, only want type based ones from LSP
        "editor.wordBasedSuggestions": false
    },
    "editor.codeLensFontFamily": "'Fira Code', 'monospace', monospace",
    "editor.fontWeight": "450",// Fira Code Retina
    "editor.fontFamily": "'Fira Code', 'Droid Sans Mono', 'monospace', monospace",
    "editor.fontLigatures": true, // nicer symbols for >=, ->, ...
    "editor.fontSize": 19,
    "editor.semanticHighlighting.enabled": true,
    "editor.tokenColorCustomizations": {
        "[Zenburn]": {
            "textMateRules": [
                {
                    "scope": "comment.doc.ocaml",
                    "settings": {
                        // makes doc comments more readable
                        "foreground": "#C2E4C2", // OKHSL adjusted to L 88.51
                    }
                },
            ],
        },
    },
    "extensions.autoUpdate": false,
    "extensions.experimental.affinity": {
        "asvetliakov.vscode-neovim": 1, // reduce lag, run in own process
        "ms-toolsai.jupyter": 2,
        "ms-toolsai.jupyter-renderers": 2,
        "ms-python.python": 2
    },
    "extensions.supportUntrustedWorkspaces": {
        "asvetliakov.vscode-neovim": { // use neovim everywhere
            "supported": true
        },
    },
    "json.schemaDownload.enable": false,
    "jupyter.themeMatplotlibPlots": true,
    "markdown-preview-enhanced.previewTheme": "github-dark.css",
    "markdown-preview-enhanced.revealjsTheme": "black.css",
    "npm.fetchOnlinePackageInfo": false,
    "redhat.telemetry.enabled": false,
    "search.useGlobalIgnoreFiles": true, // I have a global ~/.csvignore
    "update.mode": "manual",
    "vscode-neovim.neovimClean": true, // do not load neovim config/plugins
    "workbench.cloudChanges.autoResume": "off",
    "workbench.cloudChanges.continueOn": "off",
    "workbench.colorCustomizations": {
         // higher contrast
         // Note that there is a separate 'higher contrast Zenburn',
         // but that one doesn't match the Vim colors well
         "[Zenburn]": {
            "editor.background": "#1f1f1f"
         }
    },
    "workbench.enableExperiments": false,
    "workbench.preferredDarkColorTheme": "Zenburn",
    "debug.console.fontSize": 19,
    "markdown.preview.fontSize": 19,
    "terminal.integrated.fontSize": 19,
    "interactiveSession.editor.fontSize": 19,
    "files.watcherExclude": {
        "**/_build/**": true // avoid watching dune build dir: speedup
    },
    "markdown.preview.fontFamily": "\"Source Serif Pro\", -apple-system, BlinkMacSystemFont, 'Segoe WPC', 'Segoe UI', system-ui, 'Ubuntu', 'Droid Sans', sans-serif",
    "jupyter.disableJupyterAutoStart": true,

    // scrollback can be very large sometimes, avoid storing it
    "terminal.integrated.enablePersistentSessions": false,

    "typescript.disableAutomaticTypeAcquisition": true,

    // these are off by default in Codium, but be explicit to share config with VSCode
    "telemetry.telemetryLevel": "off",
    "workbench.settings.enableNaturalLanguageSearch": false,
    "explorer.excludeGitIgnore": true,
    "editor.semanticTokenColorCustomizations": {
        "[Zenburn]": {
            "enabled": true,
            "rules": {
                "namespace": { "foreground": "#efefaf", "bold": true }//Structure
                ,"function": {"foreground": "#efef8f"} // Function
                ,"variable": {"foreground": "#efdcbc"} // Identifier
            }
        }
    },
    "workbench.colorTheme": "Zenburn",
    "editor.bracketPairColorization.enabled": false,
    "debug.disassemblyView.showSourceCode": false,
    "ltex.language": "en-GB", // not consistent and interferes with semantic
}
